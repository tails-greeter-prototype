#!/usr/bin/env python3
#
# Copyright 2015 Tails developers <tails@boum.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

################################################################################
# TODO
#
# * help button should not look like buttons
# * settings change position when added, then removed
# * fill formats with formats (cf gnome-control-center)
# * show time when timezone is selected (we'll need to have it editable at some poit)
# * add a select mode for adding/removing privacy settings
# * add an icon representing encrypted storage
################################################################################

import os
import sys

import gi

gi.require_version('GLib', '2.0')
from gi.repository import GLib
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import greeterdata

# To ease future internationalisation
_ = lambda x: x

UI_FILE = 'greeter.ui'
ICON_DIR = 'icons/'
APPLICATION_ICON_NAME = 'gdm-setup'
APPLICATION_TITLE = 'Welcome to Tails!'

class GreeterMainWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title=APPLICATION_TITLE, application=app)
        self._app = app
        self.set_icon_name(APPLICATION_ICON_NAME)
        self._build_ui()
        self.set_position(Gtk.WindowPosition.CENTER)
        self._last_selected_setting_row = []

    def _build_ui(self):
        # Load UI interface definition
        self._builder = Gtk.Builder()
        self._builder.add_from_file(UI_FILE)
        self._builder.connect_signals(self)

        # Map interface objects
        box_main = self._builder.get_object('box_main')

        box_language = self._builder.get_object('box_language')
        box_language_header = self._builder.get_object('box_language_header')
        frame_language = self._builder.get_object('frame_language')
        self.label_language_header_status = \
                self._builder.get_object('label_language_header_status')
        checkbutton_language_save = \
                self._builder.get_object('checkbutton_language_save')
        self.listbox_language = self._builder.get_object('listbox_language')
        self.listboxrow_text = self._builder.get_object('listboxrow_text')
        self.label_text_value = self._builder.get_object('label_text_value')
        self.listboxrow_keyboard = self._builder.get_object('listboxrow_keyboard')
        self.label_keyboard_value = self._builder.get_object('label_keyboard_value')
        self.listboxrow_formats = self._builder.get_object('listboxrow_formats')
        self.label_formats_value = self._builder.get_object('label_formats_value')
        self.listboxrow_tz = self._builder.get_object('listboxrow_tz')
        self.label_tz_value = self._builder.get_object('label_tz_value')

        self.checkbutton_storage_show_passphrase = \
                self._builder.get_object('checkbutton_storage_show_passphrase')
        self.button_storage_configure = \
                self._builder.get_object('button_storage_configure')
        self.button_storage_state = self._builder.get_object('button_storage_state')
        self.image_storage_state = self._builder.get_object('image_storage_state')
        self.button_storage_spinner = \
                self._builder.get_object('button_storage_spinner')
        self.spinner_storage_unlock = \
                self._builder.get_object('spinner_storage_unlock')
        self.entry_storage_passphrase = \
                self._builder.get_object('entry_storage_passphrase')
        self.button_storage_lock = self._builder.get_object('button_storage_lock')
        self.button_storage_unlock =self._builder.get_object('button_storage_unlock')
        self.box_storage_unlock = self._builder.get_object('box_storage_unlock')

        box_settings = self._builder.get_object('box_settings')
        box_settings_header = self._builder.get_object('box_settings_header')
        box_settings_values = self._builder.get_object('box_settings_values')
        checkbutton_settings_save = \
                self._builder.get_object('checkbutton_settings_save')
        self.listbox_settings = self._builder.get_object('listbox_settings')
        self.listboxrow_settings_admin = \
                self._builder.get_object('listboxrow_settings_admin')
        self.label_settings_admin_value = \
                self._builder.get_object('label_settings_admin_value')
        self.listboxrow_settings_macspoof = \
                self._builder.get_object('listboxrow_settings_macspoof')
        self.label_settings_macspoof_value = \
                self._builder.get_object('label_settings_macspoof_value')
        self.listboxrow_settings_network = \
                self._builder.get_object('listboxrow_settings_network')
        self.label_settings_network_value = \
                self._builder.get_object('label_settings_network_value')
        self.listboxrow_settings_camouflage = \
                self._builder.get_object('listboxrow_settings_camouflage')
        self.label_settings_camouflage_value = \
                self._builder.get_object('label_settings_camouflage_value')

        label_settings_default = \
                self._builder.get_object('label_settings_default')
        self.toolbutton_settings_remove = \
                self._builder.get_object('toolbutton_settings_remove')
        self.toolbutton_settings_add = \
                self._builder.get_object('toolbutton_settings_add')

        self.listbox_add_setting = self._builder.get_object('listbox_add_setting')

        box_text_popover = self._builder.get_object('box_text_popover')
        searchentry_text = self._builder.get_object('searchentry_text')
        liststore_text = self._builder.get_object('liststore_text')
        treeview_text = self._builder.get_object('treeview_text')
        box_keyboard_popover = self._builder.get_object('box_keyboard_popover')
        searchentry_keyboard = self._builder.get_object('searchentry_keyboard')
        liststore_keyboard = self._builder.get_object('liststore_keyboard')
        treeview_keyboard = self._builder.get_object('treeview_keyboard')
        box_formats_popover = self._builder.get_object('box_formats_popover')
        searchentry_formats = self._builder.get_object('searchentry_formats')
        liststore_formats = self._builder.get_object('liststore_formats')
        treeview_formats = self._builder.get_object('treeview_formats')
        box_tz_popover = self._builder.get_object('box_tz_popover')
        searchentry_tz = self._builder.get_object('searchentry_tz')
        liststore_tz = self._builder.get_object('liststore_tz')
        treeview_tz = self._builder.get_object('treeview_tz')

        self.box_admin_popover = self._builder.get_object('box_admin_popover')
        self.box_admin_password= self._builder.get_object('box_admin_password')
        self.entry_admin_password = self._builder.get_object('entry_admin_password')
        self.box_admin_verify = self._builder.get_object('box_admin_verify')
        self.entry_admin_verify = self._builder.get_object('entry_admin_verify')
        self.button_admin_disable = self._builder.get_object('button_admin_disable')
        self.box_macspoof_popover = self._builder.get_object('box_macspoof_popover')
        switch_macspoof = self._builder.get_object('switch_macspoof')
        self.box_network_popover = self._builder.get_object('box_network_popover')
        self.listbox_network_controls = \
                self._builder.get_object('listbox_network_controls')
        self.listboxrow_network_clear = \
                self._builder.get_object('listboxrow_network_clear')
        self.image_network_clear = \
                self._builder.get_object('image_network_clear')
        self.listboxrow_network_specific = \
                self._builder.get_object('listboxrow_network_specific')
        self.image_network_specific = \
                self._builder.get_object('image_network_specific')
        self.listboxrow_network_off = \
                self._builder.get_object('listboxrow_network_off')
        self.image_network_off = \
                self._builder.get_object('image_network_off')
        self.box_camouflage_popover = \
                self._builder.get_object('box_camouflage_popover')
        switch_camouflage = self._builder.get_object('switch_camouflage')

        # Set preferred size
        self.set_default_size(600, -1)

        # Add our icon dir to icon theme
        icon_theme = Gtk.IconTheme.get_default()
        icon_theme.prepend_search_path(ICON_DIR)

        # Add placeholder to settings ListBox
        self.listbox_settings.set_placeholder(label_settings_default)

        # Create HeaderBar
        headerbar = Gtk.HeaderBar()
        headerbar_sizegroup = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)

        button_restart = Gtk.Button()
        button_restart.set_label(_("Restart"))
        button_restart.connect('clicked', self.cb_button_restart_clicked)
        headerbar_sizegroup.add_widget(button_restart)
        headerbar.pack_start(button_restart)

        button_start = Gtk.Button()
        button_start.set_label(_("Start Tails"))
        Gtk.StyleContext.add_class(button_start.get_style_context(),
                                   'suggested-action')
        button_start.connect('clicked', self.cb_button_start_clicked)
        headerbar_sizegroup.add_widget(button_start)
        headerbar.pack_end(button_start)

        button_tour = Gtk.Button()
        button_tour.set_label(_("Take a Tour"))
        button_tour.connect('clicked', self.cb_button_tour_clicked)
        headerbar_sizegroup.add_widget(button_tour)
        headerbar.pack_end(button_tour)

        headerbar.show_all()

        # Align CheckBoxes to the left
        for cb in [checkbutton_language_save,
                   self.checkbutton_storage_show_passphrase,
                   checkbutton_settings_save]:
            cb.set_direction(Gtk.TextDirection.RTL)

        # Add popovers
        self.popover_text = self.__add_popover(
                self.listboxrow_text,
                box_text_popover,
                closed_cb=self.cb_popover_text_closed)
        self.popover_keyboard = self.__add_popover(
                self.listboxrow_keyboard,
                box_keyboard_popover,
                closed_cb=self.cb_popover_keyboard_closed)
        self.popover_formats = self.__add_popover(
                self.listboxrow_formats,
                box_formats_popover,
                closed_cb=self.cb_popover_formats_closed)
        self.popover_tz = self.__add_popover(
                self.listboxrow_tz,
                box_tz_popover,
                closed_cb=self.cb_popover_tz_closed)

        # Add children to ApplicationWindow
        self.add(box_main)
        self.set_titlebar(headerbar)

        # Set keyboard focus chain
        box_language.set_focus_chain([frame_language, box_language_header])
        box_settings.set_focus_chain([box_settings_values, box_settings_header])

        # Create "Add Privacy Setting" dialog
        self.dialog_add_setting = Gtk.Dialog(use_header_bar=True)
        self.dialog_add_setting.set_transient_for(self)
        self.dialog_add_setting.set_title(_("Add Privacy Setting"))
        self.dialog_add_setting.set_default_size(-1, 400)

        dialog_add_setting_sizegroup = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)

        self.button_add_setting_cancel = self.dialog_add_setting.add_button(
                _("Cancel"), Gtk.ResponseType.CANCEL)
        dialog_add_setting_sizegroup.add_widget(self.button_add_setting_cancel)

        self.button_add_setting_add = self.dialog_add_setting.add_button(
                _("Add"), Gtk.ResponseType.YES)
        Gtk.StyleContext.add_class(self.button_add_setting_add.get_style_context(),
                                   'suggested-action')
        dialog_add_setting_sizegroup.add_widget(self.button_add_setting_add)

        self.button_add_setting_add.set_visible(False)
        self.button_add_setting_back = Gtk.Button.new_with_label(_("Back"))
        self.button_add_setting_back.set_visible(False)
        self.button_add_setting_back.connect(
                'clicked', self.cb_button_add_setting_back_clicked, None)
        dialog_add_setting_sizegroup.add_widget(self.button_add_setting_back)
        self.dialog_add_setting.get_header_bar().pack_end(self.button_add_setting_back)

        self.stack_add_setting = Gtk.Stack()
        self.stack_add_setting.add_named(self.listbox_add_setting, "setting-type")
        self.listbox_add_setting.set_valign(Gtk.Align.FILL)
        self.listbox_add_setting.set_vexpand(True)
        self.stack_add_setting.set_visible(True)
        self.stack_add_setting.set_transition_type(
                Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.dialog_add_setting.get_content_area().add(self.stack_add_setting)

        # Fill language treeviews
        for language in greeterdata.languages:
            liststore_text.append(language)
        self.__fill_tv("Text Language", treeview_text)
        self.liststore_text_filtered = liststore_text.filter_new()
        self.liststore_text_filtered.set_visible_func(
                self.cb_liststore_filtered_visible_func, data=searchentry_text)
        treeview_text.set_model(self.liststore_text_filtered)

        for layout in greeterdata.layouts:
            liststore_keyboard.append(layout)
        self.__fill_tv("Keyboard Layout", treeview_keyboard)
        self.liststore_keyboard_filtered = liststore_keyboard.filter_new()
        self.liststore_keyboard_filtered.set_visible_func(
                self.cb_liststore_filtered_visible_func, data=searchentry_keyboard)
        treeview_keyboard.set_model(self.liststore_keyboard_filtered)

        for locale in greeterdata.locales:
            liststore_formats.append(locale)
        self.__fill_tv("Format", treeview_formats)
        self.liststore_formats_filtered = liststore_formats.filter_new()
        self.liststore_formats_filtered.set_visible_func(
                self.cb_liststore_filtered_visible_func, data=searchentry_formats)
        treeview_formats.set_model(self.liststore_formats_filtered)

        for timezone in greeterdata.timezones:
            liststore_tz.append(timezone)
        self.__fill_tv("Time Zone", treeview_tz)
        self.liststore_tz_filtered = liststore_tz.filter_new()
        self.liststore_tz_filtered.set_visible_func(
                self.cb_liststore_filtered_visible_func, data=searchentry_tz)
        treeview_tz.set_model(self.liststore_tz_filtered)

        # Storage configuration/unlocking
        if os.getenv('TAILS_HAS_PERSISTENCE'):
            self.button_storage_configure.set_visible(False)
            self.checkbutton_storage_show_passphrase.set_visible(True)
            self.box_storage_unlock.set_visible(True)
            self.button_storage_state.set_visible(True)
            self.entry_storage_passphrase.set_visible(True)
            self.button_storage_unlock.set_visible(True)

        # Settings popovers
        switch_macspoof.connect('notify::active', self.cb_switch_macspoof_active)
        switch_camouflage.connect('notify::active', self.cb_switch_camouflage_active)

    def setting_admin_check(self):
        password = self.entry_admin_password.get_text()
        verify = self.entry_admin_verify.get_text()
        if verify and verify == password:
            icon = 'emblem-ok-symbolic'
        elif verify and not (verify == password):
            icon = 'dialog-warning-symbolic'
        else:
            icon = None
        self.entry_admin_verify.set_icon_from_icon_name(
                    Gtk.EntryIconPosition.SECONDARY, icon)
        return (verify == password)

    def setting_admin_apply(self):
        if self.setting_admin_check():
            self.label_settings_admin_value.set_text(
                self.__get_on_off_string(self.entry_admin_password.get_text()))
            self.box_admin_password.set_visible(False)
            self.box_admin_verify.set_visible(False)
            self.button_admin_disable.set_visible(True)
            if hasattr(self, 'popover_admin'):
                self.popover_admin.set_visible(False)
            return True
        else:
            return False

    # Utility methods

    @staticmethod
    def __add_popover(widget, content, closed_cb=None):
        popover = Gtk.Popover.new(widget)
        popover.set_position(Gtk.PositionType.BOTTOM)
        popover.add(content)
        if closed_cb:
            popover.connect('closed', closed_cb)
        return popover

    @staticmethod
    def __fill_tv (name, treeview):
        assert isinstance(name, str)
        assert isinstance(treeview, Gtk.TreeView)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(name, renderer, text=1)
        treeview.append_column(column)

    @staticmethod
    def __popover_toggle(popover):
        if popover.get_visible():
            popover.set_visible(False)
        else:
            popover.set_visible(True)

    @staticmethod
    def __get_on_off_string(value):
        if value:
            return _("On")
        else:
            return _("Off")

    # Callbacks

    def cb_button_add_setting_back_clicked(self, widget, user_data=None):
        self.stack_add_setting.set_visible_child_name('setting-type')
        self.button_add_setting_back.set_visible(False)
        self.button_add_setting_add.set_visible(False)
        self.stack_add_setting.remove(
                self.stack_add_setting.get_child_by_name('setting-details'))
        return False

    def cb_button_admin_disable_clicked(self, widget, user_data=None):
        self.entry_admin_password.set_text("")
        self.entry_admin_verify.set_text("")
        self.box_admin_password.set_visible(True)
        self.box_admin_verify.set_visible(True)
        self.button_admin_disable.set_visible(False)

    def cb_button_help_clicked(self, widget, user_data=None):
        dialog = Gtk.MessageDialog(
                self,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.INFO,
                Gtk.ButtonsType.CLOSE,
                _("You would see the help page for this element."))
        dialog.run()
        dialog.destroy()
        return False

    def cb_button_restart_clicked(self, widget, user_data=None):
        self._app.quit()
        return False

    def cb_button_start_clicked(self, widget, user_data=None):
        self._app.quit()
        return False

    def cb_button_storage_configure_clicked(self, user_data=None):
        dialog = Gtk.MessageDialog(
                self,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.INFO,
                Gtk.ButtonsType.CLOSE,
                _("You would be able to set up an encrypted storage."))
        dialog.run()
        dialog.destroy()
        return False

    def cb_button_tour_clicked(self, user_data=None):
        dialog = Gtk.MessageDialog(
                self,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.INFO,
                Gtk.ButtonsType.CLOSE,
                _("You would be guided through options by a wizard."))
        dialog.run()
        dialog.destroy()
        return False

    def cb_button_storage_lock_clicked(self, widget, user_data=None):
        self.button_storage_lock.set_visible(False)
        self.checkbutton_storage_show_passphrase.set_visible(True)
        self.box_storage_unlock.set_visible(True)
        self.button_storage_state.set_visible(True)
        self.image_storage_state.set_from_icon_name('tails-locked',
                Gtk.IconSize.BUTTON)
        self.entry_storage_passphrase.set_visible(True)
        self.entry_storage_passphrase.set_sensitive(True)
        self.button_storage_unlock.set_visible(True)
        self.button_storage_unlock.set_sensitive(True)
        self.button_storage_unlock.set_label(_("Unlock"))
        return False

    def cb_button_storage_unlock_clicked(self, widget, user_data=None):
        self.checkbutton_storage_show_passphrase.set_visible(False)
        self.entry_storage_passphrase.set_sensitive(False)
        self.button_storage_unlock.set_sensitive(False)
        self.button_storage_unlock.set_label(_("Unlocking"))
        self.button_storage_state.set_visible(False)
        self.button_storage_spinner.set_visible(True)
        GLib.timeout_add_seconds(2, self.cb_timeout_storage_unlocked, user_data=None)
        return False

    def cb_checkbutton_storage_show_passphrase_toggled(self, widget, user_data=None):
        self.entry_storage_passphrase.set_visibility(widget.get_active())

    def cb_entry_admin_changed(self, editable, user_data=None):
        self.setting_admin_check()
        return False

    def cb_entry_admin_focus_out_event(self, widget, event, user_data=None):
        self.setting_admin_apply()
        return False

    def cb_entry_admin_password_activate(self, widget, user_data=None):
        self.entry_admin_verify.grab_focus()
        return False

    def cb_entry_admin_verify_activate(self, widget, user_data=None):
        self.setting_admin_apply()
        return False

    def cb_listbox_add_setting_focus(self, widget, direction, user_data=None):
        # Workaround autoselection of 1st item on focus
        self.listbox_add_setting.unselect_all()
        return False

    def cb_listbox_add_setting_row_activated(self, listbox, row, user_data=None):
        if row == self.listboxrow_settings_admin:
            box = self.box_admin_popover
        elif row == self.listboxrow_settings_macspoof:
            box = self.box_macspoof_popover
        elif row == self.listboxrow_settings_network:
            box = self.box_network_popover
        elif row == self.listboxrow_settings_camouflage:
            box = self.box_camouflage_popover
        else: # this happens when the row gets unselected
            return False
        self.stack_add_setting.add_named(box, 'setting-details')
        self.stack_add_setting.set_visible_child_name('setting-details')
        self.button_add_setting_back.set_visible(True)
        self.button_add_setting_add.set_visible(True)
        return False

    def cb_listbox_language_row_activated(self, listbox, row, user_data=None):
        if row == self.listboxrow_text:
            self.__popover_toggle(self.popover_text)
        elif row == self.listboxrow_keyboard:
            self.__popover_toggle(self.popover_keyboard)
        elif row == self.listboxrow_formats:
            self.__popover_toggle(self.popover_formats)
        elif row == self.listboxrow_tz:
            self.__popover_toggle(self.popover_tz)
        return False

    def cb_listbox_network_row_activated(self, listbox, row, user_data=None):
        if row == self.listboxrow_network_clear:
            self.image_network_clear.set_visible(True)
            self.image_network_specific.set_visible(False)
            self.image_network_off.set_visible(False)
            self.label_settings_network_value.set_text(_("Direct"))
        elif row == self.listboxrow_network_specific:
            self.image_network_specific.set_visible(True)
            self.image_network_clear.set_visible(False)
            self.image_network_off.set_visible(False)
            self.label_settings_network_value.set_text(_("Censored"))
        elif row == self.listboxrow_network_off:
            self.image_network_off.set_visible(True)
            self.image_network_specific.set_visible(False)
            self.image_network_clear.set_visible(False)
            self.label_settings_network_value.set_text(_("Off"))
        if hasattr(self, 'popover_network'):
            self.popover_network.set_visible(False)
        return False

    def cb_listbox_settings_add(self, listbox, row, user_data=None):
        self._last_selected_setting_row.append(row)
        if len(listbox.get_children()) > 0:
            self.toolbutton_settings_remove.set_sensitive(True)
        return False

    def cb_listbox_settings_remove(self, container, object, user_data=None):
        if len(container.get_children()) == 0:
            self.toolbutton_settings_remove.set_sensitive(False)
        return False

    def cb_listbox_settings_row_selected(self, listbox, row, user_data=None):
        if row:
            self._last_selected_setting_row.append(row)

    def cb_listbox_settings_row_activated(self, listbox, row, user_data=None):
        if row == self.listboxrow_settings_admin:
            self.__popover_toggle(self.popover_admin)
        elif row == self.listboxrow_settings_macspoof:
            self.__popover_toggle(self.popover_macspoof)
        elif row == self.listboxrow_settings_network:
            self.__popover_toggle(self.popover_network)
        elif row == self.listboxrow_settings_camouflage:
            self.__popover_toggle(self.popover_camouflage)
        return False

    def cb_liststore_filtered_visible_func(self, model, treeiter, searchentry):
        search_query = searchentry.get_text().lower()
        value = model.get_value(treeiter, 1).lower()
        if not search_query:
            return True
        elif search_query in value:
            return True
        else:
            return False

    def cb_popover_text_closed(self, popover, user_data=None):
        self.listbox_language.unselect_all()
        return False

    def cb_popover_keyboard_closed(self, popover, user_data=None):
        self.listbox_language.unselect_all()
        return False

    def cb_popover_formats_closed(self, popover, user_data=None):
        self.listbox_language.unselect_all()
        return False

    def cb_popover_tz_closed(self, popover, user_data=None):
        self.listbox_language.unselect_all()
        return False

    def cb_popover_admin_closed(self, popover, user_data=None):
        self.listbox_settings.unselect_all()
        return False

    def cb_popover_macspoof_closed(self, popover, user_data=None):
        self.listbox_settings.unselect_all()
        return False

    def cb_popover_network_closed(self, popover, user_data=None):
        self.listbox_settings.unselect_all()
        return False

    def cb_popover_camouflage_closed(self, popover, user_data=None):
        self.listbox_settings.unselect_all()
        return False

    def cb_searchentry_formats_search_changed(self, widget, user_data=None):
        self.liststore_formats_filtered.refilter()
        return False

    def cb_searchentry_keyboard_search_changed(self, widget, user_data=None):
        self.liststore_keyboard_filtered.refilter()
        return False

    def cb_searchentry_text_search_changed(self, widget, user_data=None):
        self.liststore_text_filtered.refilter()
        return False

    def cb_searchentry_tz_search_changed(self, widget, user_data=None):
        self.liststore_tz_filtered.refilter()
        return False

    def cb_switch_camouflage_active(self, switch, pspec, user_data=None):
        self.label_settings_camouflage_value.set_text(
                self.__get_on_off_string(switch.get_state()))
        if hasattr(self, 'popover_camouflage'):
            self.popover_camouflage.set_visible(False)

    def cb_switch_macspoof_active(self, switch, pspec, user_data=None):
        self.label_settings_macspoof_value.set_text(
                self.__get_on_off_string(switch.get_state()))
        if hasattr(self, 'popover_macspoof'):
            self.popover_macspoof.set_visible(False)

    def cb_timeout_storage_unlocked(self, user_data=None):
        self.button_storage_spinner.set_visible(False)
        self.entry_storage_passphrase.set_visible(False)
        self.button_storage_unlock.set_visible(False)
        self.image_storage_state.set_from_icon_name('tails-unlocked',
                Gtk.IconSize.BUTTON)
        self.button_storage_state.set_visible(True)
        self.button_storage_lock.set_visible(True)
        return False

    def cb_toolbutton_settings_add_clicked(self, user_data=None):
        self.stack_add_setting.set_visible_child_name('setting-type')
        self.button_add_setting_back.set_visible(False)
        self.button_add_setting_add.set_visible(False)
        response = self.dialog_add_setting.run()
        if response == Gtk.ResponseType.YES:
            row = self.listbox_add_setting.get_selected_row()
            self.listbox_add_setting.remove(row)
            self.listbox_settings.add(row)
            self.dialog_add_setting.set_visible(False)
            if row == self.listboxrow_settings_admin:
                self.stack_add_setting.remove(self.box_admin_popover)
                self.popover_admin = self.__add_popover(
                        self.listboxrow_settings_admin,
                        self.box_admin_popover,
                        closed_cb=self.cb_popover_admin_closed)
            elif row == self.listboxrow_settings_macspoof:
                self.stack_add_setting.remove(self.box_macspoof_popover)
                self.popover_macspoof = self.__add_popover(
                        self.listboxrow_settings_macspoof,
                        self.box_macspoof_popover,
                        closed_cb=self.cb_popover_macspoof_closed)
            elif row == self.listboxrow_settings_network:
                self.stack_add_setting.remove(self.box_network_popover)
                self.popover_network = self.__add_popover(
                        self.listboxrow_settings_network,
                        self.box_network_popover,
                        closed_cb=self.cb_popover_network_closed)
            elif row == self.listboxrow_settings_camouflage:
                self.stack_add_setting.remove(self.box_camouflage_popover)
                self.popover_camouflage = self.__add_popover(
                        self.listboxrow_settings_camouflage,
                        self.box_camouflage_popover,
                closed_cb=self.cb_popover_camouflage_closed)
            self.listbox_settings.unselect_all()
            if len(self.listbox_add_setting.get_children()) == 0:
                self.toolbutton_settings_add.set_sensitive(False)
            self.dialog_add_setting.set_visible(False)
        self.dialog_add_setting.set_visible(False)
        return False

    def cb_toolbutton_settings_remove_clicked(self, user_data=None):
        try:
            row = self._last_selected_setting_row.pop()
        except IndexError:
            return False
        self.listbox_settings.remove(row)
        self.listbox_add_setting.add(row)
        return False

    def cb_treeview_formats_row_activated(self, treeview, path, column, user_data=None):
        treemodel = treeview.get_model()
        value = treemodel.get_value(treemodel.get_iter(path), 1)
        self.label_formats_value.set_text(value)
        self.label_language_header_status.set_text(_("Custom settings"))
        self.popover_formats.set_visible(False)

    def cb_treeview_keyboard_row_activated(self, treeview, path, column, user_data=None):
        treemodel = treeview.get_model()
        value = treemodel.get_value(treemodel.get_iter(path), 1)
        self.label_keyboard_value.set_text(value)
        self.label_language_header_status.set_text(_("Custom settings"))
        self.popover_keyboard.set_visible(False)

    def cb_treeview_text_row_activated(self, treeview, path, column, user_data=None):
        treemodel = treeview.get_model()
        value = treemodel.get_value(treemodel.get_iter(path), 1)
        self.label_text_value.set_text(value)
        self.label_language_header_status.set_text(_("Custom settings"))
        self.popover_text.set_visible(False)

    def cb_treeview_tz_row_activated(self, treeview, path, column, user_data=None):
        treemodel = treeview.get_model()
        value = treemodel.get_value(treemodel.get_iter(path), 1)
        self.label_tz_value.set_text(value)
        self.label_language_header_status.set_text(_("Custom settings"))
        self.popover_tz.set_visible(False)

class GreeterBackgroundWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title=APPLICATION_TITLE, application=app)
        self.set_icon_name(APPLICATION_ICON_NAME)
        self.override_background_color(
                Gtk.StateFlags.NORMAL, Gdk.RGBA(0, 0, 0, 1))

class GreeterApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)
        self.set_application_id = 'org.boum.tails.greeter'

    def do_activate(self):
        backgroundwindow = GreeterBackgroundWindow(self)
        backgroundwindow.maximize()
        appwindow = GreeterMainWindow(self)
        appwindow.set_transient_for(backgroundwindow)

        if not os.getenv('DEBUG'):
            backgroundwindow.show()
        appwindow.show()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = GreeterApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
